<?php
/**
 * @file Admin settins form to Paypal Adaptive Accounts API.
 */

/**
 * Paypal Adaptive Accounts API settings form.
 */
function paypal_adaptive_accounts_api_settings_form($form, &$form_state) {
  $form = array();
  $form['basic_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Paypal Adaptive Accounts API basic settings')
  );
  $form['basic_settings']['paypal_adaptive_accounts_api_server'] = array(
    '#type' => 'radios',
    '#title' => t('Paypal Adaptive Accounts server'),
    '#options' => array(
      'sandbox' => t('Sandbox - use for testing, requires a Paypal Sandbox account'),
      'live' => t('Live - use for processing real accounts'),
    ),
    '#default_value' => variable_get('paypal_adaptive_accounts_api_server', 'sandbox'),
    '#required' => TRUE,
  );
  $form['basic_settings']['paypal_adaptive_accounts_api_debug'] = array(
    '#type' => 'radios',
    '#title' => t('Do you want to debug every action of this module?'),
    '#options' => array(
      0 => t('No'),
      1 => t('Yes'),
    ),
    '#default_value' => variable_get('paypal_adaptive_accounts_api_debug', 0),
    '#required' => TRUE,
  );
  $form['basic_settings']['api_credentials'] = array(
    '#type' => 'fieldset',
    '#title' => t('PayPal API credentials')
  );  
  $form['basic_settings']['api_credentials']['paypal_adaptive_accounts_api_username'] = array(
    '#type' => 'textfield',
    '#title' => t('API Username'),
    '#default_value' => variable_get('paypal_adaptive_accounts_api_username', 'jb-us-seller_api1.paypal.com'),
    '#required' => TRUE,
  );
  $form['basic_settings']['api_credentials']['paypal_adaptive_accounts_api_password'] = array(
    '#type' => 'textfield',
    '#title' => t('API Password'),
    '#default_value' => variable_get('paypal_adaptive_accounts_api_password', 'WX4WTU3S8MY44S7F'),
    '#required' => TRUE,
  );
  $form['basic_settings']['api_credentials']['paypal_adaptive_accounts_api_signature'] = array(
    '#type' => 'textfield',
    '#title' => t('API Signature'),
    '#default_value' => variable_get('paypal_adaptive_accounts_api_signature', 'AFcWxV21C7fd0v3bYYYRCpSSRl31A7yDhhsPUU2XhtMoZXsWHFxu-RWy'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
